package br.com.mastertech.imersivo.produceraccess.controller;

import br.com.mastertech.imersivo.produceraccess.service.AccessProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/acesso")
public class AccessProducerController {

    @Autowired
    private AccessProducerService accessProducerService;

    @PostMapping("/{cliente_id}/{porta_id}")
    public void sendAcess(@PathVariable(value = "cliente_id") Long clientId, @PathVariable(value = "porta_id") Long doorId) {
        accessProducerService.sendRandomAccess(clientId, doorId);
    }
}
