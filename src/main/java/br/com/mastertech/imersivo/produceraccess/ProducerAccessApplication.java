package br.com.mastertech.imersivo.produceraccess;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProducerAccessApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProducerAccessApplication.class, args);
	}

}
