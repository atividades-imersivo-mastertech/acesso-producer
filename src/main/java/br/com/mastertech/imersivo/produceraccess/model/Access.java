package br.com.mastertech.imersivo.produceraccess.model;

public class Access {
    private Long clientId;
    private Long doorId;
    private Boolean isAccessAllowed;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getDoorId() {
        return doorId;
    }

    public void setDoorId(Long doorId) {
        this.doorId = doorId;
    }

    public Boolean getAccessAllowed() {
        return isAccessAllowed;
    }

    public void setAccessAllowed(Boolean accessAllowed) {
        isAccessAllowed = accessAllowed;
    }
}
