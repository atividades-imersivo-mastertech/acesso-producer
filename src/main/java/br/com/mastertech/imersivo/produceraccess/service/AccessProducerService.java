package br.com.mastertech.imersivo.produceraccess.service;

import br.com.mastertech.imersivo.produceraccess.model.Access;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AccessProducerService {

    @Autowired
    private KafkaTemplate<String, Access> sender;

    public void sendRandomAccess(Long clientId, Long doorId) {
        Random random = new Random();
        Access access = new Access();
        access.setDoorId(doorId);
        access.setClientId(clientId);
        access.setAccessAllowed(random.nextBoolean());
        sender.send("kaique", "key"+doorId, access);
    }
}
